# README #

In this repo I predict gender based on click events

### How do I get set up? ###

library(ff)
library(dplyr)
library(tidyr)
library(xgboost)
library(ROCR)
library(caret)

### Important Files
Notebook: XheliliLesara.nb.html
Predictions: predictions.csv